package com.im.server.general.common.data.system;

import java.util.Date;
import java.util.List;

/**
 * date 2018-07-12 10:22:36<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
public class RoleQuery {

	private String queryText;
	private String name;// 名
	private Date startCreateTime;// 建立时间
	private Date endCreateTime;//
	private String flag;// 有效标志 1：启用 0：停用
	private List<String> outIdList;

	public String getQueryText() {
		return queryText;
	}

	public void setQueryText(String queryText) {
		this.queryText = queryText;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartCreateTime() {
		return startCreateTime;
	}

	public void setStartCreateTime(Date startCreateTime) {
		this.startCreateTime = startCreateTime;
	}

	public Date getEndCreateTime() {
		return endCreateTime;
	}

	public void setEndCreateTime(Date endCreateTime) {
		this.endCreateTime = endCreateTime;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public List<String> getOutIdList() {
		return outIdList;
	}

	public void setOutIdList(List<String> outIdList) {
		this.outIdList = outIdList;
	}
}
