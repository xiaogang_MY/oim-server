package com.im.server.general.manage.system.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.im.server.general.common.bean.system.Menu;
import com.im.server.general.common.data.system.MenuQuery;
import com.im.server.general.manage.common.annotation.PermissionMapping;
import com.im.server.general.manage.system.service.MenuService;
import com.onlyxiahui.common.message.result.ResultMessage;
import com.onlyxiahui.general.annotation.parameter.Define;
import com.onlyxiahui.general.annotation.parameter.RequestParameter;

/**
 * date 2018-06-04 14:59:44<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
@RestController
@RequestMapping("/manage/system")
public class MenuController {

	@Autowired
	MenuService menuService;

	@ResponseBody
	@PermissionMapping(name = "菜单列表", key = "/manage/system/menu/allList", superKey = "system", type = PermissionMapping.Type.menu)
	@RequestMapping(method = RequestMethod.POST, value = "/menu/allList")
	public Object allList(HttpServletRequest request) {
		ResultMessage rm = new ResultMessage();
		try {
			List<Menu> list = menuService.getAllList();
			rm.put("list", list);
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}

	@ResponseBody
	@PermissionMapping(name = "刷新菜单", key = "/manage/system/menu/refresh", superKey = "/manage/system/menu/allList")
	@RequestMapping(method = RequestMethod.POST, value = "/menu/refresh")
	public Object refresh(HttpServletRequest request) {
		ResultMessage rm = new ResultMessage();
		try {
			menuService.refresh();
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "修改菜单状态", key = "/manage/system/menu/updateFlag", superKey = "/manage/system/menu/allList")
	@RequestMapping(method = RequestMethod.POST, value = "/menu/updateFlag")
	public Object update(HttpServletRequest request,
			@Define("id") String id,
			@Define("flag") String flag) {
		ResultMessage rm = new ResultMessage();
		try {
			menuService.updateFlag(id, flag);
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}

	@ResponseBody
	@RequestParameter
	@PermissionMapping(name = "条件获取菜单", key = "/manage/system/menu/list", superKey = "system", type = PermissionMapping.Type.menu)
	@RequestMapping(method = RequestMethod.POST, value = "/menu/list")
	public Object list(HttpServletRequest request,
			@Define("menuQuery") MenuQuery menuQuery) {
		ResultMessage rm = new ResultMessage();
		try {
			List<Menu> list = menuService.list(menuQuery);
			rm.put("list", list);
		} catch (Exception e) {
			e.printStackTrace();
			rm.addError("500", "系统异常");
		}
		return rm;
	}
}
