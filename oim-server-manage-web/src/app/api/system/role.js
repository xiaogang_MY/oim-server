import service from '@/libs/service';

let role = {};

role.list = function (query, page, back) {
    var body = {
        'roleQuery': query,
        'page': page
    };
    service.postBody('/manage/system/role/list', body, back);
};

role.addOrUpdate = function (bean, menuIds, back) {
    var body = {'role': bean, 'menuIds': menuIds};
    service.postBody('/manage/system/role/addOrUpdate', body, back);
};

role.get = function (id, back) {
    var body = {
        'id': id
    };
    service.postBody('/manage/system/role/get', body, back);
};

role.delete = function (id, back) {
    var body = {
        'id': id
    };
    service.postBody('/manage/system/role/delete', body, back);
};

role.roleMenuList = function (id, back) {
    var body = {
        'id': id
    };
    service.postBody('/manage/system/role/roleMenuList', body, back);
};

role.allList = function (back) {
    var body = {};
    service.postBody('/manage/system/role/allList', body, back);
};

export default role;
